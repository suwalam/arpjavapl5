package pl.sda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootBookCatalogArpjavapl5Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootBookCatalogArpjavapl5Application.class, args);
	}

}
