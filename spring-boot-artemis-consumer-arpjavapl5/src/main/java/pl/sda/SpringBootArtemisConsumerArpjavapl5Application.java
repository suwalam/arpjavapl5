package pl.sda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootArtemisConsumerArpjavapl5Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootArtemisConsumerArpjavapl5Application.class, args);
	}

}
