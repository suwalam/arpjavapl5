package pl.sda.profile;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("!dev")
public class ConnectionProviderMain implements ConnectionProvider{

    @Override
    public String getConnection() {
        return "connection MAIN";
    }
}
