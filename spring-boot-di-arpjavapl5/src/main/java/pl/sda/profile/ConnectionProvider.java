package pl.sda.profile;

public interface ConnectionProvider {

    String getConnection();

}
