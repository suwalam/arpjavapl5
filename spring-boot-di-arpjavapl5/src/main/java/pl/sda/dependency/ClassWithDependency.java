package pl.sda.dependency;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ClassWithDependency {


    private Dependency dependency;

    public void printClassWithDependency() {
        System.out.println("In ClassWithDependency class");
        dependency.printDependency();
    }

    @Autowired
    public void setDependency(Dependency dependency) {
        this.dependency = dependency;
    }
}
