package pl.sda.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
@ConfigurationProperties(prefix = "pl.sda.collections")
public class ComponentWithPropertiesCollection {

    private String fieldA;

    private List<String> users;

    private Map<String, Integer> map;

    public void setMap(Map<String, Integer> map) {
        this.map = map;
    }

    public void setUsers(List<String> users) {
        this.users = users;
    }

    public void setFieldA(String fieldA) {
        this.fieldA = fieldA;
    }

    public void printFieldA() {
        System.out.println("Field A: " + fieldA);
    }

    public void printUsers() {
        users.forEach(System.out::println);
    }

    public void printMap() {
        map.forEach((k, v) -> System.out.println("key: " + k + " value: " + v));
    }
}
