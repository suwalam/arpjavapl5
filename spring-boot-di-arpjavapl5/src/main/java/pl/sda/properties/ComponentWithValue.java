package pl.sda.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ComponentWithValue {

    @Value("${pl.sda.example}")
    private String injectedValue;

    @Value("${pl.sda.number}")
    private Integer injectedNumber;

    public void printInjectedValues() {
        System.out.println("Injected value from application.properties: " + injectedValue + " " + injectedNumber);
    }

}
