package pl.sda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import pl.sda.dependency.ClassWithDependency;

@SpringBootApplication
public class SpringBootDiArpjavapl5Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootDiArpjavapl5Application.class, args);
	}

}
