package pl.sda.runner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pl.sda.cycle.ClassA;
import pl.sda.dependency.ClassWithDependency;
import pl.sda.primary.SimpleLogger;
import pl.sda.profile.ConnectionProvider;
import pl.sda.properties.ComponentWithPropertiesCollection;
import pl.sda.properties.ComponentWithValue;
import pl.sda.scope.RandomNumberReader1;
import pl.sda.scope.RandomNumberReader2;

@Component
public class ApplicationRunner implements CommandLineRunner {

    @Autowired
    private ClassWithDependency classWithDependency;

    @Autowired
    private ClassA classA;

    @Autowired
    private ComponentWithValue componentWithValue;

    @Autowired
    private ComponentWithPropertiesCollection componentWithPropertiesCollection;

    @Autowired
    private RandomNumberReader1 randomNumberReader1;

    @Autowired
    private RandomNumberReader2 randomNumberReader2;

    @Autowired
    private ConnectionProvider connectionProvider;

    @Autowired
    private SimpleLogger simpleLogger;

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Hello from ApplicationRunner");

        classWithDependency.printClassWithDependency();

        componentWithValue.printInjectedValues();

        componentWithPropertiesCollection.printFieldA();

        componentWithPropertiesCollection.printUsers();

        componentWithPropertiesCollection.printMap();

        randomNumberReader1.printRandomNumber();

        randomNumberReader2.printRandomNumber();

        System.out.println(connectionProvider.getConnection());

        simpleLogger.printMessage("wiadomosc z klasy ApplicationRunner");
    }
}
