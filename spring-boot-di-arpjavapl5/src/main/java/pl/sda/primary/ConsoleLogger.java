package pl.sda.primary;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

//@Primary
@Component
public class ConsoleLogger implements SimpleLogger {

    @Override
    public void printMessage(String message) {
        System.out.println("ConsoleLogger: " + message);
    }
}
