package pl.sda.primary;

import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Primary
@Component
@Log4j2
public class LombokLogger implements SimpleLogger{

    @Override
    public void printMessage(String message) {
        log.info("LombokLogger: " + message);
    }
}
