package pl.sda.primary;

public interface SimpleLogger {

    void printMessage(String message);

}
