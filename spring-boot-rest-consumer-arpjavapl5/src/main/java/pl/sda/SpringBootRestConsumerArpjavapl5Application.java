package pl.sda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootRestConsumerArpjavapl5Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRestConsumerArpjavapl5Application.class, args);
	}

}
