package pl.sda.main;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import pl.sda.model.Message;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RestConsumer {

    public static void main(String[] args) throws JsonProcessingException {

        RestTemplate restTemplate = new RestTemplate();

        ObjectMapper objectMapper = new ObjectMapper();

        /*String getAllURL = "http://localhost:8080/api/messages";

        String jsonMessages = restTemplate.getForObject(getAllURL, String.class);

        System.out.println(jsonMessages);

        List<Message> messages = objectMapper.readValue(jsonMessages,
                objectMapper.getTypeFactory().constructCollectionType(List.class, Message.class));

        messages.forEach(System.out::println);*/

        //*********************************************************

        String id = "1";
//        String getByIdURL = "http://localhost:8080/api/messages/" + id; //potencjalnie niebezpieczne sklejanie parametrów
        String getByIdURL = "http://localhost:8080/api/messages/{id}";
        Map<String, String> params = new HashMap<>();
        params.put("id", id);

        Message messageById = restTemplate.getForObject(getByIdURL, Message.class, params);

        System.out.println(messageById);

        ResponseEntity<Message> responseEntity = restTemplate.getForEntity(getByIdURL, Message.class, params);
        System.out.println("Message from response entity: " + responseEntity.getBody());
        System.out.println("Status code: " + responseEntity.getStatusCode());

        responseEntity
                .getHeaders()
                .entrySet()
                .stream()
                .forEach(e -> System.out.println("header name: " + e.getKey() + ", header value: " + e.getValue()));


    }

}
