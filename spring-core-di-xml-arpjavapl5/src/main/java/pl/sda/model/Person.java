package pl.sda.model;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Person {

    private Integer id;

    private String name;

    private String surname;

}
