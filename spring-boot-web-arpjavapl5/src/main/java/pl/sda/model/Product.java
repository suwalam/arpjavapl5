package pl.sda.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Product {

    private Integer id;

    @Size(min = 3, max = 10, message = "Name size must have at least 3 characters and max 10")
    private String name;

    @DecimalMin(value = "0.0", message = "Price must be positive", inclusive = false)
    private Double price;

}
