package pl.sda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootWebArpjavapl5Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootWebArpjavapl5Application.class, args);
	}

}
