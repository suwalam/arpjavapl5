package pl.sda.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import pl.sda.model.Message;
import pl.sda.service.MessageService;

import javax.servlet.http.PushBuilder;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class MessageControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private MessageService messageService;

    @Test
    public void shouldGetAllMessages() throws Exception {

        List<Message> allMessages = messageService.getAllMessages();
        Message messageFromMemory = allMessages.get(0);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/messages")
                .contentType("application/json")
        )//zakończenie perform
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", Matchers.equalTo(messageFromMemory.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].text", Matchers.equalTo(messageFromMemory.getText())))
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(allMessages.size())));
    }

    @Test
    public void shouldGetMessageWithGivenId() throws Exception {
        Message messageFromMemory = messageService.getAllMessages().get(0);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/messages/" + messageFromMemory.getId())
                        .contentType("application/json")
                )//zakończenie perform
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.equalTo(messageFromMemory.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.text", Matchers.equalTo(messageFromMemory.getText())));
    }

    @Test
    public void shouldDeleteMessageWithGivenId() throws Exception {
        Message messageFromMemory = messageService.getAllMessages().get(0);

        mockMvc.perform(MockMvcRequestBuilders.delete("/api/messages/" + messageFromMemory.getId())
                        .contentType("application/json")
                )//zakończenie perform
                .andExpect(MockMvcResultMatchers.status().isOk());

        Assertions.assertNull(messageService.getMessageById(messageFromMemory.getId()));
    }

    @Test
    public void shouldAddMessage() throws Exception {
        //given
        Message testMessage = new Message(2, "message-2");

        //when
        mockMvc.perform(MockMvcRequestBuilders.post("/api/messages/")
                .contentType("application/json")
                .content(getMessageAsJson(testMessage))
        )//zakończenie perform
                .andExpect(MockMvcResultMatchers.status().isCreated());

        //then
        Message messageFromMemory = messageService.getMessageById(testMessage.getId());
        Assertions.assertNotNull(messageFromMemory);
        Assertions.assertEquals(testMessage.getId(), messageFromMemory.getId());
        Assertions.assertEquals(testMessage.getText(), messageFromMemory.getText());
    }

    @Test
    public void shouldUpdateMessage() throws Exception {
        //given
        List<Message> allMessages = messageService.getAllMessages();
        Message messageFromMemory = allMessages.get(0);

        Message updatedMessage = new Message(messageFromMemory.getId(), messageFromMemory.getText() + " updated");
        int messagesSizeBeforeUpdate = allMessages.size();

        //when
        mockMvc.perform(MockMvcRequestBuilders.put("/api/messages/")
                .contentType("application/json")
                .content(getMessageAsJson(updatedMessage))
        )//zakończenie perform
                .andExpect(MockMvcResultMatchers.status().isNoContent());

        //then
        messageFromMemory = messageService.getMessageById(updatedMessage.getId());
        Assertions.assertNotNull(messageFromMemory);
        Assertions.assertEquals(updatedMessage.getId(), messageFromMemory.getId());
        Assertions.assertEquals(updatedMessage.getText(), messageFromMemory.getText());
        Assertions.assertEquals(messagesSizeBeforeUpdate, messageService.getAllMessages().size());
    }

    private String getMessageAsJson(Message message) {
        try {
            return new ObjectMapper().writeValueAsString(message);
        } catch (JsonProcessingException e) {
            return "";
        }
    }

}