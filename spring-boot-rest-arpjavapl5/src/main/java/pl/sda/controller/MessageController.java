package pl.sda.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.sda.model.Message;
import pl.sda.service.MessageService;

import java.util.List;

@Slf4j
//@Controller
//@ResponseBody//wymusza na Springu zamianę obiektu zwracanego przez metodę na format JSON
//@RestController oznacza odpowiednik dwóch powyższych adnotacji
@RestController
public class MessageController {

    @Autowired
    private MessageService messageService;

    @ResponseStatus(HttpStatus.OK)//200
    @GetMapping("/api/messages")
    public List<Message> getAll() {
        return messageService.getAllMessages();
    }

    @ResponseStatus(HttpStatus.OK) //200
    @GetMapping("/api/messages/{id}")
    public Message getById(@PathVariable Integer id) {
        log.info("Get message by id: " + id);
        return messageService.getMessageById(id);
    }
//poniższa implementacja jest równoważna powyższej
//    @GetMapping("/api/messages/{id}")
//    public ResponseEntity<Message> getById(@PathVariable Integer id) {
//        Message message = messageService.getMessageById(id);
//        return new ResponseEntity<>(message, HttpStatus.OK);
//    }

    @ResponseStatus(HttpStatus.CREATED) //201
    @PostMapping("/api/messages")
    public void create(@RequestBody Message message) {
        messageService.addMessage(message);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)//204
    @PutMapping("/api/messages")
    public void update(@RequestBody Message message) {
        messageService.updateMessage(message);
    }

    @ResponseStatus(HttpStatus.OK)//200
    @DeleteMapping("/api/messages/{id}")
    public void delete(@PathVariable Integer id) {
        messageService.deleteMessage(id);
    }



}
